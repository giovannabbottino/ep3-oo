class CreatePessoas < ActiveRecord::Migration[5.2]
  def change
    create_table :pessoas do |t|
      t.string :nome
      t.string :nomeUsuario
      t.string :senha

      t.timestamps
    end
  end
end
