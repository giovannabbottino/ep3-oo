class CreateEventos < ActiveRecord::Migration[5.2]
  def change
    create_table :eventos do |t|
      t.string :nome
      t.string :descricao
      t.string :local
      t.string :data
      t.string :horarioInicial
      t.string :horarioFinal
      t.string :palestrante
      t.string :organizador

      t.timestamps
    end
  end
end
