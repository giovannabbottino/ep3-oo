class CreateAtividades < ActiveRecord::Migration[5.2]
  def change
    create_table :atividades do |t|
      t.string :nome
      t.string :descricao
      t.string :duracao
      t.string :palestrante

      t.timestamps
    end
  end
end
