json.extract! atividade, :id, :nome, :descricao, :duracao, :palestrante, :created_at, :updated_at
json.url atividade_url(atividade, format: :json)
