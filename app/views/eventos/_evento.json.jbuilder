json.extract! evento, :id, :nome, :descricao, :local, :data, :horarioInicial, :horarioFinal, :palestrante, :organizador, :created_at, :updated_at
json.url evento_url(evento, format: :json)
